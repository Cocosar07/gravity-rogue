extends KinematicBody2D

export(int) var health = 10
export(PackedScene) var hit_point_label
export(NodePath) var player_path
export(float) var acceleration = 80
var player_node
var velocity = Vector2()

func _ready():
	if player_path:
		player_node = get_node(player_path)

func _physics_process(delta):
	if player_node:
		velocity += (player_node.global_position - position).normalized() * acceleration * delta
	move_and_slide(velocity)
	velocity *= 0.9

func take_damages(amount):
	health -= amount
	if health <= 0:
		queue_free()
	on_hit(amount)

func on_hit(damages):
	$AnimationPlayer.play("hit")
	var label = hit_point_label.instance()
	label.rect_global_position = global_position
	label.get_node("Label").text = "-" + str(damages)
	label.get_node("AnimationPlayer").play("disappear")
	get_node("..").add_child(label)