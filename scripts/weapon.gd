extends RigidBody2D

export(float) var damages = 1
export(float) var max_speed = 50

func _on_weapon_body_entered(body):
	var current_damages = int(linear_velocity.length() * damages / max_speed)
	body.take_damages(current_damages)
	body.velocity = linear_velocity