extends KinematicBody2D


export(bool) var on_device = false

export(float) var max_speed = 50
export(float) var max_joystick_move = 30
export(NodePath) var joystick_path
var joystick_node

var speed = 0

var velocity = Vector2()
var touch_origin = Vector2()

func _ready():
	if joystick_path:
		joystick_node = get_node(joystick_path)
	else:
		joystick_node = null

func _unhandled_input(event):
	if event is InputEventScreenTouch:
		if event.is_pressed():
			if joystick_node:
				joystick_node.position = event.position
				joystick_node.show()
			touch_origin = event.position
		else:
			if joystick_node:
				joystick_node.hide()
			velocity = Vector2()
	elif event is InputEventScreenDrag:
		velocity = event.position - touch_origin
		speed = min(velocity.length() * max_speed / max_joystick_move, max_speed)
		velocity = velocity.normalized()
		if joystick_node:
			joystick_node.get_node("current").global_position = event.position

func _physics_process(delta):
	move_and_slide(velocity * speed)

func _process(delta):
	var movement
	if on_device:
		var accelero = Input.get_accelerometer()
		movement = Vector2(accelero.x, -accelero.y).normalized()
	else:
		movement = (get_global_mouse_position() - global_position).normalized()

	$Area2D.gravity_vec = movement