extends KinematicBody2D

var velocity = Vector2()
var is_open = false

func _physics_process(delta):
	move_and_slide(velocity)
	velocity *= 0.9

func take_damages(amount):
	if not is_open:
		on_hit()
		is_open = true
	

func on_hit():
	$Sprite.region_rect.position.y += 16
	$AnimationPlayer.play("hit")